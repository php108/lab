<?php

class Forma{
    public $method;
    public $action;
    public $submit;
    private $dropDown = '';
    private $checkbox = '';
    private $button = '';
    private $textBox = '';
    private $submitButton = '';

    function __construct($method, $action, $submit){
        $this->$method = $method;
        $this->$action = $action;
        $this->$submit = $submit;
    }

    function createCheckbox($key, $value){
        $this->checkbox .= 
        
  '<input type="checkbox" id="'.$key.'" name="'.$key.'" value="'.$value.'">
  <label for="'.$key.'">'.$value.'</label><br>';

        return $this->checkbox .= '';
    }

    function createDropDownList($key, $value){
        $this->dropDown .=  '<option value="' .$key. '">' .$value. '</option>';
        return $this->dropDown .= '';
    }

    function createButton($name, $value){
        $this->button .= '<input type="radio" name="'.$name.'" value="'.$value.'"><label>'.$value.'</label><br>';
        return $this->button .= '';
    }

    function createTextBox($key, $value){
        $this->textBox .= '<label for="'.$key.'">'.$value.'</label> <input type="text" id="'.$key.'" name="'.$key.'"><br><br>';
        return $this->textBox .= '';
    }

    function createSubmitButton($value){
        $this->submitButton .= '<input type="submit" value="'.$value.'">';
        return $this->submitButton .= '';
    }

}

$chbox1 = new Forma("prva", "drugi", "submit");
$chbox2 = new Forma("prva", "drugi", "submit");
$chbox3 = new Forma("prva", "drugi", "submit");
$chbox4 = new Forma("prva", "drugi", "submit");
$dpdown1 = new Forma("prva", "drugi", "submit");
$dpdown2 = new Forma("prva", "drugi", "submit");
$dpdown3 = new Forma("prva", "drugi", "submit");
$dpdown4 = new Forma("prva", "drugi", "submit");
$dpdown5 = new Forma("prva", "drugi", "submit");
$dpdown6 = new Forma("prva", "drugi", "submit");
$dpdown7 = new Forma("prva", "drugi", "submit");
$button1 = new Forma("prva", "drugi", "submit");
$button2 = new Forma("prva", "drugi", "submit");
$textBox1 = new Forma("prva", "drugi", "submit");
$textBox2 = new Forma("prva", "drugi", "submit");
$textBox3 = new Forma("prva", "drugi", "submit");
$submitButton = new Forma("prva", "drugi", "submit");

echo $chbox1->createCheckbox("vehicle1", "Bicikl");
echo $chbox2->createCheckbox("vehicle2", "Čamac");
echo $chbox3->createCheckbox("vehicle3", "Auto");
echo $chbox4->createCheckbox("vehicle4", "Brod");
echo "<br>";
?>

<select>
<?php 
echo $dpdown1->createDropDownList("day1", "Monday");
echo $dpdown2->createDropDownList("day2", "Tuesday");
echo $dpdown3->createDropDownList("day3", "Wednesday");
echo $dpdown4->createDropDownList("day4", "Thursday");
echo $dpdown5->createDropDownList("day5", "Friday");
echo $dpdown6->createDropDownList("day6", "Saturday");
echo $dpdown7->createDropDownList("day7", "Sunday");
?>
</select>
<pre></pre>
<?php
echo "<br>";
echo $button1->createButton("gender1", "muško");
echo $button2->createButton("gender2", "žensko");
echo "<br>";

echo $textBox1->createTextBox("ime", "Ime: ");
echo $textBox2->createTextBox("prezime", "Prezime: ");
echo $textBox3->createTextBox("OIB", "OIB: ");
echo "<br>";

echo $submitButton->createSubmitButton("Pošalji");

?>
